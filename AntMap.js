const Map = () => {
    const map = [
    ]

    let maxCol = 3, maxRow = 3
    const loadMap = (row = 3, col = 3, rand = false) => {
        for (let i = 0; i < row; i++) {
            map.push([])
            for (let j = 0; j < col; j++)
                map[i].push(rand ? (Math.random() > 0.9 ? 1 : 0) : 0)
        }
        maxCol = col
        maxRow = row
    }

    const loadJson = (data) => {
        maxCol = data.col
        maxRow = data.row
        data.map.map(e => map.push(e))
    }

    let _ant = []
    return {
        load: loadMap,
        json: loadJson,
        init: (initialPos, direction) => _ant = [Math.floor(initialPos / maxRow), initialPos % maxRow, direction, 1],
        move: () => {
            let nextPos = []
            switch (_ant[2]) {
                case 0: // North
                    nextPos = [_ant[0] - 1, _ant[1]]
                    break;
                case 1: // Est
                    nextPos = [_ant[0], _ant[1] + 1]
                    break;
                case 2: // South
                    nextPos = [_ant[0] + 1, _ant[1]]
                    break;
                case 3: // West
                    nextPos = [_ant[0], _ant[1] - 1]
                    break;
            }
            if (nextPos[0] < 0 || nextPos[0] >= maxRow || nextPos[1] < 0 || nextPos[1] >= maxCol || map[nextPos[0]][nextPos[1]] != 0) return false;
            _ant[0] = nextPos[0]
            _ant[1] = nextPos[1]
            _ant[3] *= 7 - 1

            if (nextPos[0] == maxRow - 1 && nextPos[1] == maxCol - 1) {
                console.log("Félicitation! Envoyez votre code à l'adresse <", _ant[3] + "@parkingmap.fr",">")
                process.exit(0)
            }
            return true;
        },
        left: () => _ant[2] = (_ant[2] + 7) % 4,
        rigth: () => _ant[2] = (_ant[2] + 1) % 4,
        show: () => {
            console.log(_ant)
        }
    }
}

exports.Map = Map()