## The Ant Game

Le jeu Ant Game de Parking Map est un jeu de simulation de déplacement d'une fourmie.

## Regle du jeu :

Une fourmie est placée aléatoirement dans une carte ( tableau 2 dimensions ), elle se déplace toujours d'une case dans la direction vers laquelle elle est orientée.

Elle peut changer de direction en faisant un quart de tour vers la droite ou la gauche.

Lorsqu'elle rencontre un obstacle (limite de la carte), elle ne peut plus avancer et peut seulement tourner.

La fourmie doit rejoindre la sortie correspondant à la case située à l'extrémité bas-droite de la carte.

## Map :

Vous trouverez dans index.js l'implémentation de Map contenant les fonctions nécessaires à la réussite de l'exercice.

Plus précisément, trois fonctions de déplacement vous sont fournies par Map:
    
    - move() : Déplace la fourmi d'une case dans la direction vers laquelle elle est orientée. Si un obstacle est rencontré, la fonction renvoie false et la fourmie reste sur sa position initiale.
    - left() : Change l'orientation de la fourmie d'un quart de tour vers la gauche.
    - right() : Change l'orientation de la fourmie d'un quart de tour vers la droite.


## Représentation de la carte

La carte est représentée par un tableau à deux dimensions.

Par exemple:

Le tableau: [[0, 0, 0], [0, 0, 0], [0, 0, 0]]

Correspond à la carte:

![picture](Map.png)

X représente la sortie.

La fourmie peut démarrer sur n'importe quelle case excepté celle de la sortie.

## Que faire ?

Il vous ai demandé d'implémenter l'algorithme permettant à la fourmie de rejoindre la sortie dans la fonction run().
Lorsque vous validez l'exercice, un message vous indiquant la prochaine étape à suivre s'affichera.


## How to run 
il faut installer les dependances node
```js
yarn
```

apres il faut lancer
```js
yarn dev
```

### With docker
vous pouvez utiliser un container docker pour cela il suffit juste de lancer

```shell
./dev.sh
```


